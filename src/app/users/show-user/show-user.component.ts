import { Component, OnInit } from '@angular/core';
import { UsersService } from './../users.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { MessagesService } from '../../messages/messages.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.css']
})
export class ShowUserComponent implements OnInit {

  // Instace Variables
  users;
  usersKeys;
  length_keys;
  service:UsersService;
  user_id;
  messages;
  messagesKeys;
  length;



  constructor(service:UsersService, private route: ActivatedRoute, private router: Router, private serviceMsg:MessagesService, private spinnerService: Ng4LoadingSpinnerService) {
     this.service = service;        
     this.route.paramMap.subscribe((params: ParamMap) => {
        this.user_id = params.get('id');
        this.service.getUserById(this.user_id).subscribe(response=>{
          this.users = response.json();
  		  this.usersKeys = Object.keys(this.users);   
  		  this.length_keys = this.usersKeys.length;                             
        }); 

        this.spinnerService.show();
        serviceMsg.getMessageByUser(this.user_id).subscribe(response=>{      
  	      this.messages = response.json();
  	      this.messagesKeys = Object.keys(this.messages);    
  	      this.length = this.messagesKeys.length;
          this.spinnerService.hide();     
  	    });        
    });
  }


  ngOnInit() {
  }

}
