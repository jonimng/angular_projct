import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  token;
  user_id;
  show;


  constructor(private router:Router){
  	this.token   = localStorage.getItem('token');
  	this.user_id = localStorage.getItem('user_id');
  	this.show = false;

  	if(this.token && this.user_id){
  		this.show = true;
  	}
  }

  logout(){ 
    localStorage.removeItem('token');
    localStorage.removeItem('user_id');
    this.router.navigate(['/login']);    
    this.show = false;  
  }

  ngOnInit(){}

}
