import { FormControl,FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { LoginService } from "../login/login.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  invalid = false;
  loginform = new FormGroup({
	  username:new FormControl(),
	  password:new FormControl(),	    
  });


  login(){
    this.service.login(this.loginform.value).subscribe(response=>{
      let token   = response.json().token;
      let user_id   = response.json().user_id;      
      if (token && user_id){
        localStorage.setItem('token', token);
        localStorage.setItem('user_id', user_id);
      }
      this.router.navigate(['/users']);
    },
    error=>{this.invalid= true;})
  }  

  constructor(private service:LoginService, private router:Router) { }

  ngOnInit() {    
  	var value = localStorage.getItem('token');    
  	
  	if (!value || value == undefined || value == "" || value.length == 0){		
  		this.router.navigate(['/login']);  
  	}else{
      this.router.navigate(['/users']);  		
  	}
  }

}